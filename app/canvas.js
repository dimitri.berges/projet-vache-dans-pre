var padding = 20;

/**
 * Permet d'afficher le pré dans le canvas. Le champs est défini par la position de l'ensemble de piquets saisis par l'utilasateur
 */
function draw( ) {
    canvas = document.getElementById( 'myCanvas' );
    if ( canvas.getContext ) {

        var pre = canvas.getContext( '2d' );

        pre.clearRect( 0, 0, canvas.width, canvas.height );
        pre.lineWidth = 4;
        coords = getAvailablesValues( );

        pre.beginPath( );
        currentPoint = mapCoords( coords[ 0 ], canvas.width, canvas.height, padding );
        pre.moveTo( currentPoint.x, currentPoint.y );
        for ( var i = 1; i < coords.length; i++ ) {
            currentPoint = mapCoords( coords[ i ], canvas.width, canvas.height, padding );
            pre.lineTo( currentPoint.x, currentPoint.y );
        }
        pre.closePath( );
        pre.strokeStyle = 'red';
        pre.stroke( );
        drawCow( );
    }
}

/**
 * Permet d'afficher la vache (un point noir) dans le canvas au centre de gravité du pré
 */
function drawCow( ) {
    var canvas = document.getElementById( 'myCanvas' );
    var gravity = calculateGravite( );

    if ( canvas.getContext ) {
        var cow = canvas.getContext( '2d' );
        cow.lineWidth = "10";
        cow.fillStyle = "black";
        cow.beginPath( );
        currentCowPosition = mapCoords( gravity, canvas.width, canvas.height, padding );
        cow.arc( currentCowPosition.x, currentCowPosition.y, 10, 0, 2 * Math.PI );
        cow.fill( );
    }
}
